from bs4 import BeautifulSoup
import sys
import requests

args = sys.argv[1:]


def get_html(urls):
    for url in urls:
        r = requests.get(url)
        soup = BeautifulSoup(r.content, 'lxml')
        print(soup.prettify())


get_html(args)

