FROM python:3.6-jessie

WORKDIR /opt
ADD / /opt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ENTRYPOINT python -u /opt/main.py $url